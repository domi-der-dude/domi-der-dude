<?php
include "components/header.php";
?>


<h1>SVG Logo Animation
</h1>

    <div id="drawing">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 841.9 595.3">
            <path class="logo-1-background" fill="#FF0000" d="M630.5 203.2H249.8L237.2 44.8l203-9.5 203 9.5z"/>

            <path class="logo-1-outline-top" fill="#01DF3A"
                  d="M632.3 205.2H248L235 42.9l205.2-9.6h.1l205.1 9.6-13.1 162.3zm-380.6-4h377L641 46.7l-200.8-9.4-200.8 9.4 12.3 154.5z"/>

            <path class="logo-1" fill="#4B1A5D"
                  d="M438.2 35.3h4v167.8h-4zM574.04 41.693l4 .016-.67 161.5-4-.016zM514 38.8l-2 41.8-2-41.8zM487.3 203.4l-1.9-31.9 5.8 31.4zM531.6 203l4.5-35-.5 35.3z"/>

            <path class="logo-1" fill="#4B1A5D"
                  d="M639 99.2l-63.6-.2 63.5-3.8zM329.7 40.44l15.208 162.599-3.983.372-15.208-162.598zM260.7 80.4l4.3 66.7-8.3-66.3zM387.4 71.1L386 168l-2.6-96.9z"/>

            <path class="logo-1" fill="#FFF" d="M249.8 203.2l24.7 44.6 330 3.2 26-47.8z"/>

            <path class="logo-1" fill="#4B1A5D"
                  d="M605.7 253h-1.2l-331.2-3.2-26.9-48.6h387.4L605.7 253zm-330-7.2l327.6 3.1 23.8-43.8H253.2l22.5 40.7z"/>

            <path class="logo-1" fill="#4B1A5D"
                  d="M344.8 202.4l16.5 46.3-20.2-44.8zM442.1 203.6l-13 45.7 9.1-46.6zM577.2 204l-23.4 47 19.8-48.6z"/>

            <g>
                <path class="logo-1" fill="#FFF" stroke="#4B1A5D" stroke-width="4" stroke-miterlimit="10"
                      d="M249.8 363.1h380.7l12.7 158.4-203 9.5-203-9.5z"/>

                <path class="logo-2" fill="none" stroke="#4B1A5D" stroke-width="4" stroke-miterlimit="10"
                      d="M440.2 531V363.1M332.8 524.6l.6-161.5"/>

                <path class="logo-3" fill="#4B1A5D" d="M634 427.5l-31.9 1.8 31.4-5.8zM632 410.2l-35.2-3.1 35.3-.9z"/>

                <path class="logo-1" fill="#4B1A5D"
                      d="M273 393.7l-4.3 66.7.3-66.9zM472 393.7l-4.3 66.7.3-66.9zM385.5 363.4l-1.4 97-2.6-96.9z"/>

                <path class="logo-1" fill="#FFF" stroke="#4B1A5D" stroke-width="4" stroke-miterlimit="10"
                      d="M630.5 363.1l-24.7-44.6-330-3.2-26 47.8z"/>

                <path class="logo-1" fill="#4B1A5D"
                      d="M535.5 363.9L519 317.7l20.2 44.7zM438.2 362.7l13-45.7-9.1 46.6zM303.1 362.3l23.4-47-19.7 48.7z"/>
            </g>
        </svg>    </div>

<?php
include "components/footer.php";
?>