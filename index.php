<?php
include "components/header.php";
?>



<div class="domi-wrapper text-center">
    <div class="domi-wrapper__logo">
        <img src="img/ddd1.jpg" alt="domi der dude logo">
        <h1 class="domi--h1 text-center">comming soon</h1>
        <img src="img/ddd2.jpg" alt="domi der dude logo">
    </div>
    <div class="domi-wrapper__logo-description">designer | developer | dude</div>
    <div class="domi-wrapper__logo-social">
        <a href="https://github.com/domi-der-dude" target="_blank">github</a>
        <a href="https://codepen.io/domi-der-dude/" target="_blank">codepen</a>
        <a href="https://stackoverflow.com/users/9069231/domi-der-dude" target="_blank">stackoverflow</a>
    </div>

</div>


<?php
include "components/footer.php";
?>